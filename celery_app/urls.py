from django.urls import path
from .views import SchedulerView, TaskView, AsyncTaskView

# (url,view,method,user_verify)

urls = (
    ('celery/scheduler/interval/select/', SchedulerView().interval_list, 'POST', True),
    ('celery/scheduler/interval/insert/', SchedulerView().interval_insert, 'POST', True),
    ('celery/scheduler/crontab/select/', SchedulerView().crontab_list, 'POST', True),
    ('celery/scheduler/crontab/insert/', SchedulerView().crontab_insert, 'POST', True),

    ('celery/task/get_tasks/', TaskView().get_tasks, 'POST', True),
    ('celery/task/insert/', TaskView().insert, 'POST', True),
    ('celery/task/update/', TaskView().update, 'POST', True),
    ('celery/task/delete/', TaskView().delete, 'POST', True),
    ('celery/task/list/', TaskView().list, 'POST', True),
    ('celery/task/status/change', TaskView().change_status, 'POST', True),

    ('celery/async_task/result/', AsyncTaskView().get_result, 'POST', True),
    ('celery/async_task/result/list/', AsyncTaskView().result_list, 'POST', True),

)

urlpatterns = [path(i[0], i[1]) for i in urls]
