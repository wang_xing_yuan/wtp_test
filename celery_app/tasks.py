from celery import shared_task
import time
import random


@shared_task(name='测试任务')
def test_task(**kwargs):
    delay = random.randint(1, 5)
    time.sleep(delay)
    return 'kwargs:{},delay:{} 任务完成'.format(kwargs, delay)
