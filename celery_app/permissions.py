permissions = [
    {
        'id': 30001,
        'app': '任务调度',
        'name': '时间表配置',
        'operate': '查看',
    },
    {
        'id': 30002,
        'app': '任务调度',
        'name': '时间表配置',
        'operate': '修改',
    },
    {
        'id': 30003,
        'app': '任务调度',
        'name': '定时任务',
        'operate': '查看',
    },
    {
        'id': 30004,
        'app': '任务调度',
        'name': '定时任务',
        'operate': '修改',
    },
    {
        'id': 30005,
        'app': '任务调度',
        'name': '任务结果',
        'operate': '查看',
    }
]
