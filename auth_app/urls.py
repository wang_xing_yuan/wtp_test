from django.urls import path
from .views import AuthView, RoleView, PermissionView, LogView, UserView

# (url,view,method,user_verify)

urls = (
    ('auth/login/', AuthView().login, 'POST', False),
    ('auth/logout/', AuthView().logout, 'POST', True),
    ('auth/change_pwd/', AuthView().change_password, 'POST', True),

    ('user/user_list/', UserView().user_list, 'POST', True),
    ('user/update/', UserView().update, 'POST', True),

    ('role/create_roles/', RoleView().create_role, 'POST', True),
    ('role/delete_roles/', RoleView().delete_role, 'POST', True),
    ('role/role_list/', RoleView().role_list, 'POST', True),
    ('role/get_roles/', RoleView().get_role, 'POST', True),
    ('role/update_role_user', RoleView().update_role_user, 'POST', True),

    ('permission/get_permissions/', PermissionView().get_permission, 'POST', True),
    ('permission/update_permission_role/', PermissionView().update_permission_role, 'POST', True),

    ('log/log_list/', LogView().log_list, 'GET', True),
)

urlpatterns = [path(i[0], i[1]) for i in urls]
