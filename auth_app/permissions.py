permissions = [
    {
        'id': 10001,
        'app': '用户管理',
        'name': '用户管理',
        'operate': '查看',
    },
    {
        'id': 10002,
        'app': '用户管理',
        'name': '用户管理',
        'operate': '修改',
    },
    {
        'id': 10003,
        'app': '用户管理',
        'name': '角色管理',
        'operate': '查看',
    },
    {
        'id': 10004,
        'app': '用户管理',
        'name': '角色管理',
        'operate': '修改',
    },
    {
        'id': 10005,
        'app': '用户管理',
        'name': '用户日志',
        'operate': '查看',
    }
]
