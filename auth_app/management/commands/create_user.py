from django.core.management.base import BaseCommand
from auth_app.models import UserModel
import traceback


class Command(BaseCommand):
    help = '创建用户,输入用户名、密码、是否为超级管理员'

    def _create_user(self, user_name, user_pwd, super: int):
        obj = {
            'usr_name': user_name,
            'usr_pwd': user_pwd,
        }
        if super == 1:
            UserModel.objects.create_super_user(**obj)
        else:
            UserModel.objects.create_user(**obj)

    def add_arguments(self, parser):
        parser.add_argument('name', type=str)
        parser.add_argument('password', type=str)
        parser.add_argument('super', type=int)

    def handle(self, *args, **options):
        print(options)
        try:
            self._create_user(
                user_name=options['name'],
                user_pwd=options['password'],
                super=options['super']
            )
            print('创建成功')
        except:
            print(traceback.format_exc())
