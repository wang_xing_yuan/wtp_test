FROM python:3.7
ENV PYTHONUNBUFFERED 1
COPY pip.conf /root/.pip/pip.conf

RUN mkdir -p /Alpha
WORKDIR /Alpha
ADD . /Alpha
RUN pip install -r requirements.txt


RUN chmod +x ./deploy.sh
RUN sed -i 's/\r//' ./deploy.sh
