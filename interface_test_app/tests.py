from utils.test_case import test_result
import requests

# Create your tests here.
with open('../token', 'r', encoding='UTF-8') as f:
    token = f.read().replace('\n', '')


class Test:
    def __init__(self):
        self.host = 'http://127.0.0.1:8000/'
        self.headers = {'User-Token': token}

    def info(self):
        r = requests.request(**{
            'url': self.host + 'interface/case/info/',
            'method': 'POST',
            'headers': self.headers,

        })
        test_result(r)

    def create(self):
        r = requests.request(**{
            'url': self.host + 'interface/case/create/',
            'method': 'POST',
            'headers': self.headers,
            'json': {
                'name': 'autotest_interface_testcase2',
                'desc': '自动化测试项目接口_2',
                'protocol': 'HTTP',
                'server': '',
                'url': '/config/project/list',
                'method': 'POST',
                'headers': [
                    {
                        'key': 'User-Agent',
                        'value': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
                                 'AppleWebKit/537.36 (KHTML, like Gecko) '
                                 'Chrome/88.0.4324.104 Safari/537.36'
                    }
                ],
                'params_data': [],
                'param_data': [],
                'json_data': [
                    {
                        'key': 'page',
                        'value': '1'
                    },
                    {
                        'key': 'page_size',
                        'value': '10'
                    }
                ],
                'assert_status': [
                    {
                        'expr': '-',
                        'type': 'digital',
                        'relation': '=',
                        'expectation': '200'
                    },
                    {
                        'expr': '-',
                        'type': 'string',
                        'relation': '=',
                        'expectation': '200'
                    },
                    {
                        'expr': '-',
                        'type': 'digital',
                        'relation': '>',
                        'expectation': '100'
                    }
                ],
                'assert_headers': [
                    {
                        'expr': 'abc',
                        'type': 'string',
                        'relation': '=',
                        'expectation': '123'
                    },
                    {
                        'expr': 'Content-Type',
                        'type': 'string',
                        'relation': '=',
                        'expectation': 'application/json'
                    }
                ],
                'assert_json': [
                    {
                        'expr': 'code',
                        'type': 'digital',
                        'relation': '=',
                        'expectation': '123'
                    },
                    {
                        'expr': 'data',
                        'type': 'string',
                        'relation': '=',
                        'expectation': '123'
                    },
                    {
                        'expr': 'err_msg',
                        'type': 'string',
                        'relation': '=',
                        'expectation': ''
                    }
                ],
            },
        })
        test_result(r)

    def update(self):
        r = requests.request(**{
            'url': self.host + 'interface/case/update/',
            'method': 'POST',
            'headers': self.headers,
            'json': {
                'id': 3,
                'name': 'test_name1qwe',
                'desc': 'eqweqwe',
                'protocol': 'HTTPS',
                'url': '/abc/asd/sad',
                'method': 'POST',
                'headers': [],
                'param_data': [],
                'verify_status': 200,
                'verify_json': [{
                    'expr': None,
                    'type': 'string',
                    'relation': '=',
                    'expectation': '123'
                }],
            },
        })
        test_result(r)

    def delete(self):
        r = requests.request(**{
            'url': self.host + 'interface/case/delete/',
            'method': 'POST',
            'headers': self.headers,
            'json': {
                'id': 2,
            },
        })
        test_result(r)

    def list(self):
        r = requests.request(**{
            'url': self.host + 'interface/case/list/',
            'method': 'POST',
            'headers': self.headers,
            'json': {
                'page': 1,
                'page_size': 10
            },
        })
        test_result(r)

    def detail(self):
        r = requests.request(**{
            'url': self.host + 'interface/case/detail/',
            'method': 'POST',
            'headers': self.headers,
            'json': {
                'id': 6
            },
        })
        test_result(r)

    def execute_case(self):
        r = requests.request(**{
            'url': self.host + 'interface/execute/case/',
            'method': 'POST',
            'headers': self.headers,
            'json': {
                'id': 3,
                'env_id': 1
            },
        })
        test_result(r)


if __name__ == '__main__':
    t = Test()
    t.create()
