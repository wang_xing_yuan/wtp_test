'''
(变量,注释)
'''


class Const:
    '''
    常量类
    接收常量字段结构如下Field
    复写value，_value包含所有自定义常量
    '''
    # Field=('id','name')
    value = []
    _value = []

    def __str__(self):
        return ','.join(self._value)

    def get_id_or_name(self, id_or_name):
        for item in self.value:
            (i, j) = item
            if i == str(id_or_name):
                return j
            if j == str(id_or_name):
                return i


# 请求方法
class Method(Const):
    GET = ('GET', 'GET')
    POST = ('POST', 'POST')
    DELETE = ('DELETE', 'DELETE')
    HEAD = ('HEAD', 'HEAD')
    PUT = ('PUT', 'PUT')
    PATCH = ('PATCH', 'PATCH')

    value = [GET, POST, DELETE, HEAD, PUT, PATCH]
    _value = [i[0] for i in value]


# 请求协议
class Protocol(Const):
    HTTP = ('HTTP', 'http')
    HTTPS = ('HTTPS', 'https')

    value = [HTTP, HTTPS]
    _value = [i[0] for i in value]


# 数据类型
class DataType(Const):
    String = ('string', '字符串')
    Digital = ('digital', '数字')
    Bool = ('bool', '布尔值')  # True,False,None

    value = [String, Digital, Bool]
    _value = [i[0] for i in value]
    bool_list = ['true', 'false', 'null', 'none']  # todo 异常3

    def check_type(self, type, value):
        if type == self.String[0]:
            if isinstance(value, str):
                return True
        elif type == self.Digital[0]:
            if isinstance(value, int) or isinstance(value, float):
                return True
        elif type == self.Bool[0]:
            if isinstance(value, bool):
                return True

    def get_type(self, value):
        if isinstance(value, str):
            return self.String[1]
        elif isinstance(value, int) or isinstance(value, float):
            return self.Digital[1]
        elif isinstance(value, bool):
            return self.Bool[1]

    def transform_type(self, type, value: str):
        # 将字符串转换为对应类型
        if type == self.String[0]:
            return str(value)
        elif type == self.Digital[0]:
            return float(value)
        elif type == self.Bool[0]:
            if value.lower() not in self.bool_list:
                raise Exception('布尔值类型不存在,请选择 {} 其中之一'.format(','.join(self.bool_list)))
            if value.lower() == 'true':
                return True
            elif value.lower() == 'false':
                return False
            elif value.lower() == 'none' or value.lower() == 'null':
                return None


# 断言类型
class AssertType(Const):
    StatusCode = ('response_status', '响应状态码')
    ResHeaders = ('response_headers', '响应头')
    JsonBody = ('response_json_body', 'json响应体')

    value = [StatusCode, ResHeaders, JsonBody]
    _value = [i[0] for i in value]


# 断言关系
class AssertRelation(Const):
    Less = ('<', '小于')
    Equal = ('=', '等于')
    More = ('>', '大于')
    N_Equal = ('!=', '不等于')

    value = [Less, Equal, More, N_Equal]
    _value = [i[0] for i in value]

    def _less(self, v1, v2):
        # 判断v1小于v2
        # v1,v2需要时数字，才有效，否则返回失败
        if not isinstance(v1, (int, float)) and isinstance(v2, (int, float)):
            return False
        return v1 < v2

    def _more(self, v1, v2):
        if not isinstance(v1, (int, float)) and isinstance(v2, (int, float)):
            return False
        return v1 > v2

    def _equal(self, v1, v2):
        return v1 == v2

    def _n_equal(self, v1, v2):
        return v1 != v2

    def calculate(self, v1, relation, v2):
        d = {
            self.Less[0]: self._less,
            self.Equal[0]: self._equal,
            self.More[0]: self._more,
            self.N_Equal[0]: self._n_equal
        }
        try:
            result = d[relation](v1, v2)  # todo 异常2
        except:
            raise Exception('{}未知的断言关系'.format(relation))
        return result
