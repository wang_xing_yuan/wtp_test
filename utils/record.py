from auth_app.views import LogView


def record(request, action: int, target_obj=None):
    LogView.record_log(request, action, target_obj)
