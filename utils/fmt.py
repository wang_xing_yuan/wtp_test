import datetime

_datetime_format = '%Y-%m-%d %H:%M:%S'
_date_format = '%Y-%m-%d'


def datetime_str(DateTime: datetime.datetime):
    if DateTime:
        return DateTime.strftime(_datetime_format)


def date_str(Date: datetime.date):
    if Date:
        return Date.strftime(_date_format)


def datetime_now():
    return datetime_str(datetime.datetime.now())


if __name__ == '__main__':
    print(datetime_now())
