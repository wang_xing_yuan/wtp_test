from functools import wraps
from .messages import Insufficien_Permissions


# 权限id在permission_cache.json中查看

def permission_verify(permission_id):
    def decorator(func):
        @wraps(func)
        def verify(self, request):
            usr_permission = request.USER_INFO['permissions']
            if (permission_id in usr_permission) or ('*' in usr_permission):
                return func(self, request)
            return Insufficien_Permissions()

        return verify

    return decorator
