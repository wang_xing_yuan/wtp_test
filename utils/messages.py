from django.http import JsonResponse
import json

'''
code: int
err_msg: str
msg: str
data: dict or list

请求成功,处理成功 200
请求成功,处理失败 201

请求异常 500
表单(参数)验证失败 501
请求方式错误 502
权限不足 503
用户验证失败 504

'''

json_response = JsonResponse


# 后期加入json编辑器


def res_msg(code=200, msg='', err_msg='', data=[]):
    return {
        'code': code,
        'msg': msg,
        'err_msg': err_msg,
        'data': data
    }


def Json_Response(code=200, msg='', err_msg='', data=[]):
    return json_response(res_msg(
        code=code,
        msg=msg,
        err_msg=err_msg,
        data=data
    ))


def Error_Res(exception: Exception):
    return json_response(
        res_msg(
            code=500,
            err_msg=str(exception)
        )
    )


# 表单验证错误
def Form_Invalid(form):
    # form:表单实例
    return json_response(res_msg(
        code=501,
        err_msg=json.dumps(dict(form.errors), separators=(',', ':'), ensure_ascii=False),
    ))


# 请求方式错误
def Err_Req_Method():
    return json_response(res_msg(
        code=502,
        err_msg='请求方式错误'
    ))


# 权限不足
def Insufficien_Permissions():
    return json_response(res_msg(
        code=503,
        err_msg='权限不足'
    ))


# 用户状态失效
def Err_Auth_Verify():
    return json_response(res_msg(
        code=504,
        err_msg='登录信息已过期'
    ))
