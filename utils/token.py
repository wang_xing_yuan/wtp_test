import jwt
import base64
from Alpha.settings import SECRET_KEY

algorithm = 'HS256'


def encode_token(payload: dict):
    jwt_body = jwt.encode(
        payload=payload,
        key=SECRET_KEY,
        algorithm=algorithm
    )
    token = base64.b64encode(jwt_body.encode()).decode()
    return token


def decode_token(token: str):
    jwt_body = base64.b64decode(token.encode()).decode()
    payload = jwt.decode(
        jwt=jwt_body,
        key=SECRET_KEY,
        algorithms=algorithm
    )
    return payload
