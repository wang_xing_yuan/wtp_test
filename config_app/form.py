from django import forms
from utils.form import BaseForm
from utils.field import ListField


class DatabaseForm:
    class Insert(BaseForm):

        host = forms.CharField()
        port = forms.IntegerField()
        user = forms.CharField()
        password = forms.CharField()
        database = forms.CharField()
        desc = forms.CharField()
        type = forms.CharField()

        def valid_type_field(self, data):
            if 'type' in data.keys():
                if data['type'] not in ['mysql', 'oracle']:
                    raise forms.ValidationError({'database': '请选择mysql or oracle'})

        def clean(self):
            data = self.cleaned_data
            self.valid_type_field(data)
            self.set_operate_user(data, 'create_by')
            return data

    class Delete(BaseForm):
        id = forms.IntegerField()

    class Update(Insert):
        id = forms.IntegerField()

        def clean(self):
            data = self.cleaned_data
            self.valid_type_field(data)
            self.set_operate_user(data, 'update_by')
            return data

    class Select(BaseForm):
        page = forms.IntegerField(min_value=1)
        page_size = forms.IntegerField(min_value=1)

    class Connect(BaseForm):
        id = forms.IntegerField()


class ProjectForm:
    class Insert(BaseForm):
        proj_name = forms.CharField()
        proj_desc = forms.CharField()
        proj_members = ListField()

    class Update(Insert):
        id = forms.IntegerField()

    class Delete(BaseForm):
        id = forms.IntegerField()

    class List(BaseForm):
        page = forms.IntegerField()
        page_size = forms.IntegerField()


class EnvForm:
    class Insert(BaseForm):
        name = forms.CharField()
        host = forms.CharField()
        port = forms.IntegerField()

        def clean(self):
            pass

    class Update(Insert):
        id = forms.IntegerField()

    class List(BaseForm):
        page = forms.IntegerField()
        page_size = forms.IntegerField()

    class Delete(BaseForm):
        id = forms.IntegerField()
