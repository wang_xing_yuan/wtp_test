from celery import Celery
from .settings import celery_config
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Alpha.settings')

app = Celery()

app.conf.update(**celery_config)

app.autodiscover_tasks()

# celery -A Alpha worker -l info
# celery -A Alpha beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler

# celery flower -A Alpha --port=5555
